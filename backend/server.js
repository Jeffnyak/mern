import express from 'express';

import dotenv from 'dotenv';
import userRoutes from './routes/userRoute.js'
import { notFound, errorHandler } from './middlewares/errorMiddleware.js';
import cookieParser from 'cookie-parser';
import connectDB from './config/db.js';
dotenv.config();
connectDB();

const PORT = process.env.PORT || 4000;

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(cookieParser());
app.use("/api/users", userRoutes);

app.get('/', (req, res) => res.send("Server is running"));

app.use(notFound);
app.use(errorHandler);

app.listen(PORT, ()=>console.log(`App is running on ${PORT}`));