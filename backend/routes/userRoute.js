import express from 'express';
import { 
        authUser,
        registerUser,
        logoutUser,
        userProfile,
        updateProfile 
} from '../controllers/userController.js';
import { protectRoute } from '../middlewares/authMiddleware.js';
const router = express.Router();

router.post("/auth", authUser);
router.post("/register", registerUser);
router.post("/logout", logoutUser);
router.route("/profile").get(protectRoute,userProfile).put(protectRoute, updateProfile)

export default router;