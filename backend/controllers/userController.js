import asyncHandler from 'express-async-handler';
import User from '../models/userModel.js';
import generateToken from '../utils/generateToken.js';

// @desc  Auth user / set token;
// route  POST/api/users/auth
// @access Public
const authUser = asyncHandler(async (req, res) =>{
    const {email, password} = req.body;
    const user = await User.findOne({ email });

    if(user && (await user.matchPassword(password))){
        generateToken(res, user._id);
        res.status(201).json({
            _id: user._id,
            name: user.name,
            email: user.email
        })
    }else{
        res.status(401);
        throw new Error("Invalid email or password");
    }
});

// @desc  Regsiter a new user
// route  POST/api/users/
// @access Public
const registerUser = asyncHandler(async (req, res) =>{
    const { name, email, password } = req.body;
    const emailExists = await User.findOne({ email });
    if(emailExists){
        throw new Error("User already exist");
    };

    const user = await User.create({
        name, email, password
    });

    if(user){
        generateToken(res, user._id);
        res.status(201).json({
            _id: user._id,
            name: user.name,
            email: user.email
        })
    }else{
        res.status(400);
        throw new Error("Invalid User Data");
    }
});

// @desc  Get user profile
// route  GET/api/users/profile
// @access Private
const userProfile = asyncHandler(async (req, res) =>{
    const user = {
        _id: req.user._id,
        name: req.user.name,
        email:req.user.email
    }
    res.status(200).json({user:user});
});

// @desc  Update user profile
// route  POST/api/users/profile
// @access Private
const updateProfile = asyncHandler(async (req, res) =>{
    const user = await User.findById(req.user._id);
    if(user){
        user.name = req.body.name || user.name;
        user.email = req.body.email || user.email;

        if(req.body.password){
            user.password = req.body.password;
        }

        const updatedInfo = await user.save();
        res.status(201).json({
            _id: updatedInfo._id,
            name: updatedInfo.name,
            email: updatedInfo.email
        });
    
    }else{
        res.status(400);
        throw new Error("User not found");
    }
    res.status(200).json({message: "Update User Profile"})
});

// @desc  Auth user logout;
// route  POST/api/users/logout
// @access Private
const logoutUser = asyncHandler(async (req, res) =>{
    res.cookie('jwt', '', {
        httpOnly: true,
        expires: new Date(0)
    })
    res.status(200).json({message: "User logged out succesfully"});
});


export {
    authUser,
    registerUser,
    logoutUser,
    userProfile,
    updateProfile
}