import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import {createRoutesFromElements, Route, Routes, RouterProvider, createBrowserRouter} from 'react-router-dom';
import HomeScreen from './screens/HomeScreen.jsx';
import LoginScreen  from './screens/LoginScreen.jsx';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<App/>} >
      <Route path='/' element={<LoginScreen />}/>
      <Route path='/login' element={<LoginScreen />}/>
    </Route>
  )
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
