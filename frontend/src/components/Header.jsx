/* eslint-disable no-unused-vars */
import { Navbar, Container, Nav } from 'react-bootstrap';

import { FaSignInAlt, FaSignOutAlt } from 'react-icons/fa';

import { LinkContainer } from 'react-router-bootstrap';

const Header = () =>{
    return (
        <header>
            <Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect>
                <Container>
                    <LinkContainer to='/'>
                        <Navbar.Brand>Mearn Auth</Navbar.Brand>
                    </LinkContainer>
                    <Navbar.Toggle aria-controls='basic-navbar-nav'/>
                    <Navbar.Collapse id='basic-navbar-nav'/>
                    <Nav className='ms-auto'>
                        <LinkContainer to='/login'>
                            <Nav.Link>
                                <FaSignInAlt /> Sign In
                            </Nav.Link>
                        </LinkContainer>
                        <LinkContainer to='/register'>
                            <Nav.Link>
                                <FaSignInAlt /> Register
                            </Nav.Link>
                        </LinkContainer>
                    </Nav>
                </Container>
            </Navbar>
        </header>
    )
}

export default Header;