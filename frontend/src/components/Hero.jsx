import {LinkContainer} from 'react-router-bootstrap';
import {Nav} from 'react-bootstrap';
export default function Hero() {
  return (
    <div className="py-5">
       <div className="container d-flex justify-content-center">
        <div className="card p-5 d-flex flex-column align-items-center hero-card bg-light w-75">
            <h3 className="text-center mb-4">MERN Authentication</h3>
            <p className="text-center mb-4">
            ndustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
            <div className='d-flex'>
                <LinkContainer to='/login'>
                  <Nav.Link className="btn btn-primary me-3 rounded-0" >
                      <button type="button" className='btn btn-primary btn-sm'>Login</button>
                  </Nav.Link>
                </LinkContainer>
                <LinkContainer to='/register'>
                  <Nav.Link className="btn btn-secondary me-3 rounded-0" >
                    <button type="button" className='btn btn-secondary btn-sm'>Register</button>
                  </Nav.Link>
                </LinkContainer>
            </div>
        </div>
       </div>
    </div>
  )
}
